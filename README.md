# Exercism.io - Bash track
First thing we must do is install the ``exercism`` command line utility. For that, our best option is to use the package manager of our distribution. In macOS would be:

```
$ brew install exercism
```

If you don't want to use a package manager, installing manually is quite simple, since ``exercism`` is a **single executable** (written in Go) with no additional runtime requirements. We just have to download a [release](https://github.com/exercism/cli/releases) appropriate for our distribution, uncompress the package, and place the executable somewhere in our ``$PATH``. 

**Completion scripts**: The package also includes **completion scripts** for different shells; read [here](https://github.com/exercism/cli/tree/master/shell) about the whole process.

* Homebrew installs the Bash completion files to `/usr/local/etc/bash_completion.d/`, and automatically sources them. (Maybe because the **recipe** for the main `bash-completion` is tuned to look into that folder)
* Check also the Homebrew Documentation about [Shell completion](https://docs.brew.sh/Shell-Completion)

## Configuring
Once installed, we have to configure it. First thing is to link it to our account at [Exercism.io], and for that we have to navigate to our [settings page at Exercism.io](https://exercism.io/my/settings) and copy a **CLI token** that we'll use like this:

```
$ exercism configure --token=4feb0903-1423-4c2b-asap-1t471x3f042c
```

Now we have linked our ``exercism`` command with our account at [Exercism.io], which will make things a lot easier.

### Changing our workspace
By default, our working directory is placed at our ``$HOME`` directory, but we can move it with the command:

```
$ exercism -w ~/somewhere_else
```

The **workspace** directory is important because it is where all exercises are downloaded by default.

## Using it
At [Exercism.io] we can choose a track, and have access to a list of exercises. Once we choose an exercise, we can download it to our computer by copying the command and pasting it into our command line. It looks something like this:

```
$ exercism download --exercise=hamming --track=bash
```

It will automatically be downloaded to our **workspace** directory.

[Exercism.io]: https://exercism.io/
