#!/usr/bin/env bash

main () {
  local limit=$1
  local numbers
  local primes

  # Generate the list of numbers.
  for ((i=2; i <= limit; i++))
  do
    numbers[$i]=$i
  done

  # Mark the composite numbers.
  for index in "${!numbers[@]}"
  do
    for ((j=index + index; j <= limit; j+=index))
    do
      if ((j > limit))
      then
        break
      else
        numbers[$j]='composite'
      fi
    done
  done

  # Extract the primes into a separate array
  for index in "${!numbers[@]}"
  do
    if [[ "${numbers[index]}" != "composite" ]]
    then
      primes+=($index)
    fi
  done

  printf "%s\n" "${primes[*]}"
}

main "$@"

