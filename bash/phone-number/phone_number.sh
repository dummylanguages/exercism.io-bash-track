#!/usr/bin/env bash

# North American Numbering Plan (NANP)
#
# * Country code(optional): 1 or +1
# * Area code (3 digits)
# * Local number:
#   * Exchange code (3 digits)
#   * Subscriber number (4 digits)
#
# For example: [+1] 234-456-7890

function invalid_number () {
  echo "Invalid number.  [1]NXX-NXX-XXXX N=2-9, X=0-9"
  exit 1
}

# Print the number.
function valid_ten_digits_number () {
  echo ${digits}
  exit 0
}

# Print the number without the country code.
function valid_eleven_digits_number () {
  echo ${digits:1}
  exit 0
}

main () {
  # Using Pattern matching to get rid of non-digits.
  digits=${1//[![:digit:]]}

  # If the number has less than 9 or more than 11 digits.
  if [[ ${#digits} -le 9 ]] || [[ ${#digits} -ge 12 ]]
  then
    invalid_number
  fi

  # If the number has 10 digits.
  if [[ ${#digits} -eq 10 ]]
  then
    # If the AREA CODE or EXCHANGE CODE start with 0 or 1.
    if [[ ${digits:0:1} =~ [01] ]] || [[ ${digits:3:1} =~ [01] ]]
    then
      invalid_number
    else
      valid_ten_digits_number
    fi
  fi

  # If the number has 11 digits
  if [[ ${#digits} -eq 11 ]]
  then
    # If its COUNTRY code is not with 1,
    # or its AREA or EXCHANGE codes start with 0 or 1.
    if [[ ${digits:0:1} -ne 1 ]]  ||
      [[ ${digits:1:1} =~ [01] ]] ||
      [[ ${digits:4:1} =~ [01] ]]
    then
      invalid_number
    else
      valid_eleven_digits_number
    fi
  fi
}

main "$@"

