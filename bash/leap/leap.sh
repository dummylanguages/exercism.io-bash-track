#!/usr/bin/env bash

main () {
  # Testing if user inputs more than one argument.
 [[ ${#@} != 1 ]] && { echo "Usage: leap.sh <year>"; exit 1; }

  # Testing if user inputs anything other than an Integer.
  # (The -eq operator returns true if both operands are the same Integer,
  # but use it with the POSIX-compliant test command, [ ], so it works with letters.)
  ! [ ${1} -eq ${1} ] 2> /dev/null && { echo "Usage: leap.sh <year>"; exit 1; }

  # Year divisible by 4 but not by 100.
  if (( $1 % 4 == 0 )) && (( $1 % 100 != 0 ))
  then
    echo 'true'
    exit 0

  # Year divisible by 100 and 400.
  elif (( $1 % 100 == 0 )) && (( $1 % 400 == 0 ))
  then
    echo 'true'
    exit 0
  else
    echo 'false'
    exit 0
  fi
}

main "$@"

