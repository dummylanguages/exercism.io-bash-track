#!/usr/bin/env bash

function calculate_resistance () {
  local digits=$1 # Value passed by main.
  local left_side=''
  local right_side='' # Amount of zeros or power of 10: 10^0, 10^1, 10^2...

  # Parse the digits into Left side and Right side.
  function parse_digits() {
    # If all the digits are zero, let's finish fast. 
    if [[ $digits == '000' ]]
    then
      echo '0 ohms'
      exit 0 # Exit the function.
    
    else
      right_side=${digits:2} # To begin with: the right_side is the last digit.

      # If the first digit is zero, discard it.
      if [[ ${digits:0:1} == '0' ]]
      then
        left_side=${digits:1:1} # Left side is only the 2nd digit.

      else
        # If the second digit is zero, pass it to the Right side.
        if [[ ${digits:1:1} == '0' ]]
        then
          left_side=${digits:0:1} # Left side is the first digit only.
          (( right_side++ ))

        else  # If the 2nd digit is not zero, the left_side is the first 2 digits.
          left_side=${digits:0:2}
        fi
      fi
    fi
  }

  function gimme_da_ohms () {

    # We use the value of the right side as exponent.
    if (( 10**$right_side >= 10**9 ))
    then
      ohms="$(( $left_side * (10**$right_side / 10**9) )) gigaohms"

    elif (( 10**$right_side >= 10**6 ))
    then
      ohms="$(( $left_side * (10**$right_side / 10**6) )) megaohms"

    elif (( 10**$right_side >= 10**3 ))
    then
      ohms="$(( $left_side * (10**$right_side / 10**3) )) kiloohms"

    else
      ohms="$(( $left_side * (10**$right_side) )) ohms"
    fi
    
    # Finally return the ohms.
    echo "$ohms"
  }

  # First we call the function that parse the digits into two sides.
  parse_digits $digits
  # Then we call the function that prints the final value in ohms.
  gimme_da_ohms
}

function color_to_digit () {
  local color_bands=(
    black
    brown
    red
    orange
    yellow
    green
    blue
    violet
    grey
    white
  )

  # Put here the color translation to digits.
  local digits

  # Iterating over the 3 first arguments; ignore the rest.
  for color in $1 $2 $3
  do
    # First of all: Return code 1 if invalid color.
    if ! [[ "${color_bands[*]}" == *"$color"* ]]
    then
      echo "$color is not a valid color"
      return 1
    fi

    # Loop over the array indices.
    for index in ${!color_bands[@]}
    do
      # If the color-argument matches the color at the current index.
      if [ "$color" == "${color_bands[$index]}" ]
      then
        # Build up the digits with the array indices of the matched colors.
        digits+=$index
      fi
    done
  done
  
  # Output the digits.
  echo $digits
}

main () {
  local digits
  
  if digits=$(color_to_digit "$@")
  then
    # We call the function calculate_resistance and store the result.
    value_ohms=$(calculate_resistance $digits)
  else
    echo "$digits" # digits in this case contains an error message.
    exit 1
  fi

  # Print the value in ohms and exit.
  echo $value_ohms
  exit 0
}

main "$@"

