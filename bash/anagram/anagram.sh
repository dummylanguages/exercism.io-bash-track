#!/usr/bin/env bash
main () {
  local anagram_list=''

  for word in ${*:2}
  do
    # This flag is set on EACH WORD to 'maybe'
    local anagram='maybe'

    # If the 1st argument and word are the same (Case insensitive)
    [[  ${word^^} == ${1^^} ]] && continue
    
    # If words are the same length.
    if [[ ${#word} -eq ${#1} ]]
    then
      # Anagrams are case-insensitive, plus it helps when comparing characters.
      local first_arg=${1^^}
      local word_upper=${word^^}

      # Same length. Check if the words use the same set of letters. 
      while read -n1 char
      do
        if [[ ${first_arg} == **"$char"** ]]
        then
          # If the letter is in the 'first argument' let's strip it from that
          # letter, so it's matched only once.
          first_arg=${first_arg/$char/''}
        else
          # Setting the 'anagram' flag to 'false'
          anagram='false'
          break
        fi
      done <<< $word_upper

      # Add the word to the anagrams list, with a trailing space.
      [[ $anagram != 'false' ]] && anagram_list+="$word "
    fi
  done
  
  if [[ ${#anagram_list} -gt 0 ]]
  then
    # Let's strip the trailing space at the very end and print.
    echo ${anagram_list::-1}
  else
    # Empty list, nothing to strip; let's print the empty list.
    echo $anagram_list
  fi

  # Same exit code anyways.
  exit 0
}

main "$@"

