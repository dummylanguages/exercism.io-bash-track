#!/usr/bin/env bash

main () {
  # If no argument is passed, we exit.
  [[ $1 == '' ]] && exit 0

  local RNA='' # Superfluous but explicit declaration

  while read -n 1 nucleotide
  do
    case $nucleotide in
      G)  # Guanine
        RNA+=C
        ;;
      C)  # Cytosine
        RNA+=G
        ;;
      T)  # Thymine
        RNA+=A
        ;;
      A)  # Adenine
        RNA+=U  # Uracil
        ;;
      [!GCTA])
        echo "Invalid nucleotide detected."
        exit 1
    esac
  done <<< $1

  echo $RNA
  exit 0
}

main "$@"

