#!/usr/bin/env bash

main () {
  local decimal=$1
  local handshake   # Array to store the components of the handshake.
  local components=(
    'wink'
    'double blink'
    'close your eyes'
    'jump'
  )
  local reverse # true or false

  # If the 5th bit (2^4=16) equals 1, set reverse to 'true'.
  if ((decimal >> 4 & 1 == 1))
  then
    reverse=true
  else
    reverse=false
  fi

  # We use the range items (0, 1, 2 and 3) both as exponents of base 2
  # and as indexes to access elements in the array 'components'.
  for i in {0..3}
  do
    # If 'reverse' was set, each time we get a match, 
    # we PREPEND the element to the beginning of the array.
    if $reverse
    then
      if ((decimal >> $i & 1 == 1))
      then
        # Double-quoting the array expansion is essential
        # for elements that contain spaces.
        handshake=("${components[$i]}" "${handshake[@]}")
      fi

    # If 'reverse' is false, each time we get a match, 
    # we APPEND the element to the end of the array.
    else
      if ((decimal >> $i & 1 == 1))
      then
        handshake+=("${components[$i]}")
        # Equivalent to:
        #handshake=("${handshake[@]}" "${components[$i]}")
      fi
    fi
  done

  # Small trick to print the elements as comma-separated values.
  IFS=, ; printf "%s\n" "${handshake[*]}"
}

main "$@"

