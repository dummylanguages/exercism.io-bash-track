#!/usr/bin/env bash
main () {

# Associative array. (Must be explicitely declared)
declare -Ai nucleotides # No need to use the -i flag, but it doesn't hurt.

# We also initialize it, so we have something to print
# in case the argument is an empty string.
nucleotides[A]=0
nucleotides[C]=0
nucleotides[G]=0
nucleotides[T]=0

# Iterate over the DNA strand.
while read -n1 char
do
  case $char in
    A)
      ((nucleotides[A]++))
      ;;
    C)
      ((nucleotides[C]++))
      ;;
    G)
      ((nucleotides[G]++))
      ;;
    T)
      ((nucleotides[T]++))
      ;;
    [!ACGT]) # Extended pattern mathing (shopt -s extglob)
      echo 'Invalid nucleotide in strand'
      exit 1
      ;;
  esac
done <<< ${1^^}

# Let's print the results.
for key in "${!nucleotides[@]}" # Iterating over the keys.
do
  echo "$key: ${nucleotides[$key]}"
done
}

main "$@"

