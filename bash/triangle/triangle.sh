#!/usr/bin/env bash

function check_triangle_inequality () {
  if (( $(echo "$a + $b > $c && $a + $c > $b && $b + $c > $a" | bc) ))
  then
    echo 'true'
    exit 0
  else
    echo 'false'
    exit 0
  fi
}

function equilateral () {
  # All three sides measure zero.
  if (( $(echo "$a == 0 && $b == 0 && $c == 0" | bc) ))
  then
    echo 'false'
    exit 0
  fi

  # If two sides are equal.
  if (( $(echo "$a == $b" | bc) ))
  then
    # If the third side is also equal.
    if (( $(echo "$a == $c" | bc) ))
      then
        echo 'true'
        exit 0
    else
      echo 'false'
      exit 0
    fi
  else
    echo 'false'
    exit 0
  fi
 }

function isosceles () {
  # If the three sides are equal.
  if (( $(echo "$a == $b == $c" | bc) ))
  then
    echo 'true'
    exit 0
  # If any two pair of sides are equal.
  elif (( $(echo "$a == $b || $a == $c || $b == $c" | bc) ))
  then
    check_triangle_inequality

  # If no two pair of sides are equal.
  else
    echo 'false'
    exit 0
  fi
}

function scalene () {
  # If three or two sides are equal.
  if (( $(echo "$a == $b == $c || $a == $b || $a == $c || $b == $c" | bc) ))
  then
    echo 'false'
    exit 0
  else
    check_triangle_inequality
  fi
}

main () {
  # your main function code here
  a=$2
  b=$3
  c=$4
  
  # Selector
  case $1 in
    isosceles)
      isosceles
      ;;
    equilateral)
      equilateral
      ;;
    scalene)
      scalene
      ;;
  esac

}

main "$@"

