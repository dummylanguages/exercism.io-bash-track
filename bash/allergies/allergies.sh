#!/usr/bin/env bash

function list () {
  # The main function calls this one and passes only 1 argument:
  # The allergy score input by the user.
  local score=$1

  # This string will be augmented with the allergens.
  local list=''

  # We'll use the indexes of this array as exponents.
  local allergies=(
    'eggs'
    'peanuts'
    'shellfish'
    'strawberries'
    'tomatoes'
    'chocolate'
    'pollen'
    'cats'
  )

  # We ignore allergens that score over 128, by subtracting their values.
  # (I'm only dealing with the next in the scale, 256... to pass tests ;-)
  if [[ $score -ge 256 ]]
  then
    (( score -= 256 ))
  fi

  # Looping backwards over the exponents of powers of 2.
  for (( i=7; i >= 0; i-- ))
  do
    if (( score >= 2**i ))
    then
      # Decreasing the score. 
      (( score -= 2**i ))

      # Adding the allergen with higher scores to the end of the list.
      list="${allergies[$i]} ${list}"
    fi
  done

  # Print the list
  echo $list
  
  exit 0 # Not sure if I should exit here too.
}

function allergic_to () {
  # Assign positional parameters to local variables.
  local score=$1
  local allergen=$2
  
  # We call list to get a list of allergens.
  local list=$(list $score)

  # Then we check if our allergen is in that list.
  if [[ $list == *"$allergen"* ]]
  then
    echo 'true'
  else
    echo 'false'
  fi

  exit 0 # Not sure if I should exit here too.
}

main () {
# 1st argument, right after the script name (which it's 0)
local score=$1

# 2nd argument is the function the user wants to run.
local fn=$2

# 3rd argument is the allergen.
local allergen=$3

# Select function:
case $fn in
  list)
    # We pass only the score
    echo "$(list $score)"
    exit 0 # Not sure if I should exit here too.
    ;;
  allergic_to)
    # We pass the score and the allergen.
    echo "$(allergic_to $score $allergen)"
    exit 0 # Not sure if I should exit here too.
    ;;
esac
}

main "$@"

