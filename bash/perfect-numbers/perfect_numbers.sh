#!/usr/bin/env bash

main () {
  local number=$1
  local list      # String to store the list of factors.
  local factors   # Array to store the list of factors.
  local aliquot   # Store the total sum of the factors.

  # User input validation
  if [[ $number -le 0 ]]
  then
    printf "%s" "Classification is only possible for natural numbers." 
    exit 1
  fi

  # The code below works, but it's super slow.
  #for ((i=1; i <= number/2; i++))
  #do
    # Another slow approach: if [ $(echo "$number % $i" | bc) -eq 0 ]
    #if ((number % i == 0)) && echo "sloooow"
    #then
      #factors+=($i)
    #fi
  #done

  # Build the list of factors using 'awk' (also slow but way faster).
  list=$(awk "BEGIN {
  for(i=1; i <= $number/2; i++)
    if ($number % i == 0)
      print i
  }")

  # Make the list of factors(string) into an array
  factors=($list)

  # Iterate over the list of factors to add them together.
  for f in "${factors[@]}"
  do
    ((aliquot += f))
  done
  
  # Finally, determine if the number is perfect, deficient or abundant.
  if ((aliquot == number))
  then
    printf "perfect\n"
  elif ((aliquot > number))
  then
    printf "abundant\n"
  elif ((aliquot < number))
  then
    printf "deficient\n"
  fi
}

main "$@"

