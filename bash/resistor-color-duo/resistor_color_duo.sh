#!/usr/bin/env bash

main () {
  # your main function code here
  color_bands=(
    Black
    Brown
    Red
    Orange
    Yellow
    Green
    Blue
    Violet
    Grey
    White)

  # Capitalize the arguments.
  color1=${1^}
  color2=${2^}

  # Only iterate over the first two arguments. Ignore the rest.
  for color in $color1 $color2
  do
    # If the color is invalid (it doesn't exist in the array)
    if ! [[ "${color_bands[@]}" == *"$color"* ]]
    then
      echo "invalid color"
      exit 1
    fi

    # Iterate over the indices of the array.
    for index in ${!color_bands[@]}
    do
      # Fetch the color with that index and compare with argument.
      if [ $color == ${color_bands[$index]} ]
      then
        number+=$index
      fi
    done
  done

  echo $number
  exit 0
}
main "$@"

