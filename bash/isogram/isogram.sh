#!/usr/bin/env bash

main () {
  input=$1

  # Clear the input.
  input=${input//[![:alpha:]]}

  # And downcase it.
  input=${input,,}
  
  for ((i = 0; i < ${#input}; i++))
  do
    char=${input:$i:1}

    # If the input (stripped of the current char) contains the current char.
    if [[ ${input/$char} == *"$char"* ]]
    then
      echo "false"
      exit 0
    fi
  done

  # If the loop didn't detect any repeated character, the word is an isogram.
  echo 'true'
  exit 0
}
main "$@"

