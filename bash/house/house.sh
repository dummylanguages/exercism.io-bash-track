#!/usr/bin/env bash

main () {
  # your main function code here
  local verbs=(
    'lay in'
    'ate'
    'killed'
    'worried'
    'tossed'
    'milked'
    'kissed'
    'married'
    'woke'
    'kept'
    'belonged to'
  )
  
  local nouns=(
    'house that Jack built'
    'malt'
    'rat'
    'cat'
    'dog'
    'cow with the crumpled horn'
    'maiden all forlorn'
    'man all tattered and torn'
    'priest all shaven and shorn'
    'rooster that crowed in the morn'
    'farmer sowing his corn'
    'horse and the hound and the horn'
  )

  local first=$1    # First verse boundary.
  local second=$2   # Second verse boundary.
  local inner

  # When first or second boundaries are out of range.
  if [[ $first -lt 1 ]] || [[ $first -gt 12 ]] ||
    [[ $second -lt 1 ]] || [[ $second -gt 12 ]]
    then
      echo 'These verses are invalid.'
      exit 1
  fi

  for ((j=$first; j<=$second; j++))
  do
    inner=$j

    # Decrease by 1 to adjust to zero-based array indexing.
    (( inner-- ))
    printf "This is the %s" "${nouns[$inner]}"

    # Decrease by 1 again for the next item in the list.
    (( inner-- ))

    for (( i=$inner; i >= 0; i--))
    do
      printf "\nthat %s the %s" "${verbs[$i]}" "${nouns[$i]}"
    done

    printf ".\n\n"
  done

  exit 0
}

main "$@"
