#!/usr/bin/env bash

# The following comments should help you get started:
# - Bash is flexible. You may use functions or write a "raw" script.
#
# - Complex code can be made easier to read by breaking it up
#   into functions, however this is sometimes overkill in bash.
#
# - You can find links about good style and other resources
#   for Bash in './README.md'. It came with this exercise.
#
#   Example:
#   # other functions here
#   # ...
#   # ...
#
main () {
    # your main function code here
    local string=''

    # True if the operands we are comparing are algebraically equal; meaning, both are integers
    if [ $1 -eq $1 ] 2> /dev/null ; then
        [[ "$1 % 3" -eq '0' ]] && string+='Pling'
        [[ "$1 % 5" -eq '0' ]] && string+='Plang'
        [[ "$1 % 7" -eq '0' ]] && string+='Plong'

        # If the number was divisible by 3, 5 and/or 7, string has some content.
        # Otherwise prints the positional argument as it was typed.
        echo "${string:-${1}}"
        return 0
    else   # The argument is not a number
        >2& echo "Your argument is not an integer number" 
        return 1
    fi
}
#
#   # call main with all of the positional arguments
main "$@"
#
# *** PLEASE REMOVE THESE COMMENTS BEFORE SUBMITTING YOUR SOLUTION ***
