#!/usr/bin/env bash

main () {
digits=${#1}

# Stores the total sum of powers
sum=0
for (( i=0; i < $digits; i++ ))
do
  # Stores the product of each digit
  product=1

  for (( j=0; j < $digits; j++ ))
  do
    (( product*=${1:$i:1} ))
  done

  (( sum+=$product ))
done

[[ $sum -eq $1 ]] && echo 'true' || echo 'false'
exit 0
}

main "$@"

