#!/usr/bin/env bash

main () {
  counter=0

  while read -n 1 char
  do
    case "${char^^}" in
      [AEIOULNRST] ) ((counter++));;
      [DG]         ) ((counter+=2));;
      [BCMP]       ) ((counter+=3));;
      [FHVWY]      ) ((counter+=4));;
      [K]          ) ((counter+=5));;
      [JX]         ) ((counter+=8));;
      [QZ]         ) ((counter+=10));;
    esac
  done <<< "${1}"

  echo $counter
  exit 0
}

main "$@"

