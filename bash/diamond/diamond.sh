#!/usr/bin/env bash

diamond ()
{
  local line=$1
  local letter=$2
  local i output

  for ((i=1; i <= line * 2 + 1; i++))
  do
    # Print a letter at the beginning and the end; spaces in between.
    if [[ $i -eq 1 ]] || [[ $i -eq $((line * 2 + 1)) ]]
    then
      output+="$letter"
    else
      output+=' '
    fi
  done

  printf "%s" "$output"
}

surrounding_spaces ()
{
  local i=$1
  local k

  for ((k=0; k < i; k++))
  do
    printf " "
  done
}

main ()
{
  local alphabet=({A..Z})
  local letter=$1 # The letter input by the user.
  local index     # The position of the letter in the 'alphabet' array.
  local i line

  # Getting the index of the letter that the user input
  for i in "${!alphabet[@]}"
  do
    if [[ $letter == "${alphabet[i]}" ]]
    then
      index=$i
      break
    fi
  done

  # Top of the diamond.
  for ((line=0; line <= index; line++))
  do
    surrounding_spaces $((index - line))
    diamond $line ${alphabet[line]}
    surrounding_spaces $((index - line))
    printf "\n"
  done

  # Bottom of the diamond.
  for ((line=index-1; line >= 0; line--))
  do
    surrounding_spaces $((index - line))
    diamond $line ${alphabet[line]}
    surrounding_spaces $((index - line))
    printf "\n"
  done
}

main "$@"

