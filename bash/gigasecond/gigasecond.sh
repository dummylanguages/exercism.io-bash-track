#!/usr/bin/env bash

main () {
  local date_arr
  local year month day
  local time_str
  local date_str

  # Let's parse the date string into an array; easier to parse.
  IFS='-' read -a date_arr <<< "$1"
  year="${date_arr[0]}"
  month="${date_arr[1]}"
  day="${date_arr[2]}"

  # If the user input includes time, ends up in the day string.
  if [[ $day == *"T"* ]]
  then
    time_str=${day#*T} # Trim everything up to and including the 'T'.
    day=${day%T*}      # Trim all after and including the 'T'. 

    # Build a string with the date with the trimmed day.
    date_str="${year}-${month}-${day}"
    date --date="$date_str $time_str UTC +1000000000 seconds" '+%Y-%m-%dT%T'
    exit 0
  fi

  # The user input doesn't include any time, only a date.
  date_str="${year}-${month}-${day}"
  date --date="$date_str UTC +1000000000 seconds" '+%Y-%m-%dT%T'
}

main "$@"
 
