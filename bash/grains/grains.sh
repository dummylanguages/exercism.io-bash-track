#!/usr/bin/env bash

main () {
  
  grains=0  # To keep count of the grain.
  
  if [[ $1 == 'total' ]]
  then
    counter=0 # We use it as an exponent; each square contains 2 ^ $counter.
    square=0  # It holds the amount of rice per square.

    until [ $counter -eq 64 ]
    do
      square=$( echo "2 ^ $counter" | bc)
      grains=$( echo "$grains + $square" | bc)

      (( counter++ ))
      #echo "counter: $counter, grains: $grains"
    done

    echo $grains
    exit 0

  elif  [[ $1 -eq 64 ]]
  then

    # The 64th square is 2^63; the 1st is 2^0 
    bc <<< "2 ^ ($1 -1)"
    exit 0

  elif [[ $1 -lt 1 ]] || [[ $1 -gt 64 ]]
  then
    
    echo "Error: invalid input"
    exit 1

  else  # User inputs between 1 and 63
    grains=$((2 ** ($1 - 1) ))

    echo $grains
    exit 0
  fi

}

main "$@"

