#!/usr/bin/env bash

compare_arrays ()
{
  # The received arguments must be converted to arrays again.
  local arr1=($1) 
  local arr2=($2)
  local i

  # Iterate over any of the equal-size arrays.
  for ((i=0; i < ${#arr1[@]}; i++))
  do
    # Compare each element.
    if [[ ${arr1[i]} != ${arr2[i]} ]]
    then
      # Return from the function if compared elements are different.
      return 1
    fi
  done

  # Return zero-code if both arrays are equal.
  return 0
}

main () {
  # Assigning positional parameters, with 1st character removed.
  local str1=${1#?} str2=${2#?}
  # Removing the last character (closing square bracket).
  str1=${str1%?}
  str2=${str2%?}

  local arr1 arr2
  # Convert strings to arrays.
  # The characters in $IFS are treated individually as separators so that in this
  # case fields may be separated by either a comma or a space rather than the 
  # sequence of the two characters. Interestingly though, empty fields aren't 
  # created when comma-space appears in the input because the space is treated
  # specially.
  IFS=', ' read -a arr1 <<< "$str1"
  IFS=', ' read -a arr2 <<< "$str2"

  # The length of the arrays.
  local length_list1 length_list2
  length_list1=${#arr1[@]}
  length_list2=${#arr2[@]}
  
  if ((length_list1 > length_list2))
  then
    local temp_slice    # Store a slice of list 1 to compare to list 2.
    local slices_amount # The amount of slices we can take from list 1.
    slices_amount=$(( length_list1 - length_list2 ))

    local i # We'll call compare_arrays, where another 'i' counter is used.
    # Iterate over list 1 as many times as the amount of possible slices.
    for ((i=0; i <= slices_amount; i++))
    do
      # This is a shiftable slice from the 1st array.
      # Slicing operation expands to a string, we must convert it to an array again.
      temp_slice=( "${arr1[@]:i:length_list2}" )

      # To pass the whole arrays, we must index them using the *.
      if compare_arrays "${temp_slice[*]}" "${arr2[*]}"
      then
        echo "superlist"
        exit 0
      fi
    done

    # If during the loop we didn't exit, the lists are unequal.
    echo "unequal"
    exit 0

  elif ((length_list1 == length_list2))
  then
    if compare_arrays "${arr1[*]}" "${arr2[*]}"
    then
      echo "equal"
      exit 0
    else
      echo "unequal"
      exit 0
    fi

  elif ((length_list1 < length_list2))
  then
    local temp_slice    # Store a slice of list 2 to compare to list 1.
    local slices_amount # The amount of slices we can take from list 2.
    slices_amount=$(( length_list2 - length_list1 ))

    local i # We'll call compare_arrays, where another 'i' counter is used.
    # Iterate over list 2.
    for ((i=0; i <= slices_amount; i++))
    do
      temp_slice=( "${arr2[@]:i:length_list1}" )

      if compare_arrays "${temp_slice[*]}" "${arr1[*]}"
      then
        echo "sublist"
        exit 0
      fi
    done

    echo "unequal"
    exit 0
  fi
}

main "$@"

