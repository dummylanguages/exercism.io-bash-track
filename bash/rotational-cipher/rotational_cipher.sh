#!/usr/bin/env bash

main () {
  local message=$1
  local key=$2

  # Generating an array with the alphabet letters using Brace expansion.
  local alphabet
  alphabet=({a..z})
  
  # Generating the rotated version of the alphabet in another array.
  local cipher
  for index in {0..25}
  do
    if ((key + index >= 26))
    then
      cipher[index]=${alphabet[index + key - 26]}
    else
      cipher[index]=${alphabet[key + index]}
    fi 
  done

  # A string with the encrypted message.
  local encrypted

  # Iterate over the characters of the message.
  while IFS= read -n1 char
  do
    # If it's a letter
    if [[ "$char" =~ [A-Za-z] ]]
    then
      # Iterate over the alphabet until we find the character
      for ((i=0; i <= 25; i++))
      do
        if [[ $char == "${alphabet[i]}" ]]
        then
          # Append the rotated version.
          encrypted+=${cipher[i]}
        elif [[ $char == "${alphabet[i]^}" ]]
        then
          # Append the rotated Uppercase version.
          encrypted+=${cipher[i]^}
        fi
      done
    # If the character is not a letter, append it as it is.
    else
      encrypted+="$char"
    fi
  done <<< "$message" 

  # Finally print the encoded string.
  printf "%s\n" "$encrypted"
  exit 0
}

main "$@"

