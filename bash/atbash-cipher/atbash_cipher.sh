#!/usr/bin/env bash

main () { 
  # Capture and clean user input from anything other than alphanumeric characters.
  local input=${2//[![:alnum:]]}
  input=${input,,} # All lowercase.
  local output
  local temp i j 
  local alphabet
  declare -A dict

  # Generate an array with the alphabet.
  alphabet=({a..z})

  # The index 'j' must be offset to zero-based arrays.
  for((i=0, j=${#alphabet[@]} - 1; i < ${#alphabet[@]}; i++, j--))
  do
    if [[ $1 == 'decode' ]]
    then
      dict[${alphabet[i]}]=${alphabet[j]}
    else
      dict[${alphabet[j]}]=${alphabet[i]}
    fi
  done

  while read -n1 char
  do
    # If the character is a letter, add its atbash ciphered version.
    if [[ $char =~ [[:alpha:]] ]]
    then
      output+="${dict[$char]}"
    else
      # Numbers are added as such.
      output+=$char
    fi
  done <<< $input

  # Let's output the encoded/decoded string properly formatted.
  if [[ $1 == encode ]]
  then
    while read -n5 word
    do
      temp+="$word "
    done <<< $output

    echo $temp # It swallows trailing space (Don't double quote)
  else
    printf "%s\n" "$output"
  fi
  }

main "$@"

