#!/usr/bin/env bash

main () {
  local series=$1 
  local slice_length=$2
  local amount_of_slices
  local slices  # Array with slices

  # The order of the test commands matter!
  if [[ -z $series ]]
  then
    printf "%s\n" "series cannot be empty"
    exit 1
  
  elif ((slice_length > ${#series}))
  then
    printf "%s\n" "slice length cannot be greater than series length"
    exit 1

  elif ((slice_length == 0))
  then
    printf "%s\n" "slice length cannot be zero"
    exit 1

  elif ((slice_length < 0))
  then
    printf "%s\n" "slice length cannot be negative"
  fi

  # This is how we calculate the amount of slices.
  amount_of_slices=$((${#series} - slice_length + 1))

  for ((i=0; i < amount_of_slices; i++))
  do
    slices+=( "${series:$i:$slice_length}" )
  done

  printf "%s\n" "${slices[*]}"
}

main "$@"

