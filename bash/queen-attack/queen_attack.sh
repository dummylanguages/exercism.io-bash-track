#!/usr/bin/env bash
validate_input ()
{
  local queen
  read -a queen <<< "$1"

  # Validating user input: rows.
  if (( queen[0] < 0 ))
  then
    echo 'row not positive'
    exit 1
  elif (( queen[0] > 7 ))
  then
    echo 'row not on board'
    exit 1
  fi

  # Validating user input: columns.
  if (( queen[1] < 0 ))
  then
    echo 'column not positive'
    exit 1
  elif (( queen[1] > 7 ))
  then
    echo 'column not on board'
    exit 1
  fi
}

main () {
  local white black
  local row column

  # The command 'getopts' is a Bash builtin.
  while getopts 'w:b:' option
  do
    case $option in
      w)
        IFS=',' read -a white <<< "$OPTARG"
        ;;
      b)
        IFS=',' read -a black <<< "$OPTARG"
        ;;
    esac
  done

  validate_input "${white[*]}"
  validate_input "${black[*]}"

  # More validation. Both queens cannot be on the same square.
  if ((white[0] == black[0] && white[1] == black[1]))
  then
    echo "same position"
    exit 1
  # Queens share a row or a column.
  elif ((white[0] == black[0] || white[1] == black[1]))
  then
    echo "true"
    exit 0
  fi

  # White queen is the reference; scan diagonals from its position.
  # North-west diagonal.
  for ((row=white[0], column=white[1]; row >= 0 && column >= 0; row--, column--))
  do
    if ((row == black[0] && column == black[1]))
    then
      echo "true"
      exit 0
    fi
  done

  # South-east diagonal.
  for ((row=white[0], column=white[1]; row <= 7 && column <= 7; row++, column++))
  do
    if ((row == black[0] && column == black[1]))
    then
      echo "true"
      exit 0
    fi
  done

  # North-east diagonal.
  for ((row=white[0], column=white[1]; row <= 7 && column <= 7; row--, column++))
  do
    if ((row == black[0] && column == black[1]))
    then
      echo "true"
      exit 0
    fi
  done

  # South-west diagonal.
  for ((row=white[0], column=white[1]; row <= 7 && column <= 7; row++, column--))
  do
    if ((row == black[0] && column == black[1]))
    then
      echo "true"
      exit 0
    fi
  done

  # If the for loops didn't find the Black queen, return false.
  echo 'false'
}

main "$@"

