#!/usr/bin/env bash

main () {
  # Associative array to hold the word count.
  declare -A word_count

  # Cleaning the input string in stages.
  clean_string=${1//,/\ /}  # Turn commas into spaces.
  
  # Nuke anything that is not a letter, number, \n, apostrophe or space.
  clean_string=${clean_string//[![:alpha:][:digit:]\\n\ \']}

  # Turn the string into an array of lowercase words.
  words_array=$(echo -e ${clean_string,,})
  

  # Iterating over each word in the input string.
  for word in ${words_array[@]}
  do
    # Removing surrounding single quotes.
    word=${word#\'}
    word=${word%\'}

    # If the dictionary already contains a key for that word,
    # the random word 'yep' is substituted.
    if [[ ${word_count[$word]:+yep} == 'yep' ]]
    then
      # If the key exists, increment its count.
      # We use ${parameter@operator} so that we can autoincrement
      # keys that contain apostrophes.
      (( word_count["${word@Q}"]++ ))
      
      # Another approach.
      #word_count["$word"]+=1
    else
      # If the key doesn't exist, initialize it.
      word_count[${word}]=1
    fi
  done

  for key in ${!word_count[@]}
  do
    echo "$key: ${word_count[$key]}"
  done
}
main "$@"

