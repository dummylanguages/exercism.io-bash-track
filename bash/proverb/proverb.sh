#!/usr/bin/env bash

main () {
  # If no positional arguments are passed.
  if [ ${#@} -eq 0 ]
  then
    echo "" # Print an empty line.
    exit 0
  fi

  # Iterate over all positional arguments.
  # SC2145: Argument mixes string and array. Use * or separate argument.
  # (Shellcheck prefers I use * instead of @)
  for ((i=1; i<${#*}; i++))
  do
    echo "For want of a ${*:$i:1} the ${*:$i+1:1} was lost."
  done

  echo "And all for the want of a ${*:1:1}."
}

main "$@"
