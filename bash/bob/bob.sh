#!/usr/bin/env bash

# The following comments should help you get started:
# - Bash is flexible. You may use functions or write a "raw" script.
#
# - Complex code can be made easier to read by breaking it up
#   into functions, however this is sometimes overkill in bash.
#
# - You can find links about good style and other resources
#   for Bash in './README.md'. It came with this exercise.
#
#   Example:
#   # other functions here
#   # ...
#   # ...
#
main () {
# your main function code here
    answer="$1" 

    # All uppercase and last character -> ?
    ( [[ ${answer} =~ [[:alpha:]] ]] && [[ ${answer} == ${answer^^} ]] && [[ ${answer: -1} == '?' ]] ) && { echo "Calm down, I know what I'm doing!"; exit; }

    # Last character -> ?
    ( [[ ${answer: -1} == '?' ]] || [[ "${answer}" =~ '?'[[:space:]]*$ ]] ) && { echo 'Sure.'; exit; }

    # No letters: numbers, punctuation marks, empty string...
    ( [[ -z ${answer} ]] || ! [[ ${answer} =~ [[:alnum:]] ]] ) && { echo 'Fine. Be that way!'; exit; }
    
    # All uppercase, no matter the last character.
    ( [[ ${answer} =~ [[:alpha:]] ]] && [[ ${answer} == ${answer^^} ]] ) && { echo 'Whoa, chill out!'; exit; }
    
    # Last character -> ! . 
    echo 'Whatever.'
}
#
# call main with all of the positional arguments
main "$@"
#
# *** PLEASE REMOVE THESE COMMENTS BEFORE SUBMITTING YOUR SOLUTION ***
