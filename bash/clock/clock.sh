#!/usr/bin/env bash

check_numeric_arg () {
  local arg=$1
  # Check if the argument is not an integer.
  if ! [ "$arg" -eq "$arg" ] 2> /dev/null
  then 
    printf "%s\n" "invalid arguments"
    exit 1
  fi
}

check_operator_arg () {
  if ! [[ $1 =~ [+-] ]]
  then
    printf "%s\n" "invalid arguments"
    exit 1
  fi
}

main () {
  # Verify number of arguments.
  if [ "$#" -ne 2 ] && [ "$#" -ne 4 ]
  then
    printf "%s\n" "invalid arguments"
    exit 1
  fi

  # Check if the first two arguments are integers.
  for arg in $1 $2
  do
    check_numeric_arg "$arg"
  done

  # User input was good, let's assign the variables.
  local hour=$1
  local minutes=$2
  
  shift 2
  # Once the posit. param. are shifted, check if there are more args.
  if [ $# -eq 2 ]
  then
    # Check also they are valid arguments.
    check_operator_arg "$1"
    check_numeric_arg "$2"

    # Assign the variables
    local operator=$1
    local operand=$2
    
    # Add or subtract minutes.
    if [ "$operator" == '+' ]
    then
      (( minutes += operand ))
    else
      (( minutes -= operand ))
    fi 
  fi

  # Negative minutes.
  if ((minutes < 0))
  then
    # Negative minutes roll over.
    if ((minutes <= -60))
    then
      ((hour += (minutes / 60) ))
      ((minutes = minutes % 60 )) # The minutes leftover are negative.

      # Adjust hour for the leftover minutes.
      ((hour--))
      ((minutes = 60 + minutes))

    else
      ((hour--))
      ((minutes = 60 + minutes))
    fi
  fi

  # Roll over minutes.
  if ((minutes >= 60))
  then
    ((hour += minutes / 60))
    ((minutes = minutes % 60))
  fi

  # Negative hours.
  if ((hour < 0))
  then
    # Negative hours roll over.
    if ((hour <= -24))
    then
      ((hour = hour % 24))
      ((hour = 24 + hour))

    else
      ((hour = 24 + hour))
    fi
  fi

  if ((hour == 24))
  then
    ((hour = 0))
  fi
 
  # Roll over hours.
  if ((hour > 24))
  then
    ((hour %= 24))
  fi
  printf "%02d:%02d\n" "$hour" "$minutes"
}

main "$@"

