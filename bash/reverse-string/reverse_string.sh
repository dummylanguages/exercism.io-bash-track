#!/usr/bin/env bash

main () {

# Don't use the -ge operator!! -> Infinite loop
for (( i=-1; i >= -${#1};  i-- ))
do
  # Quote to escape globbing (a possible *)
  reverse+="${1:$i:1}"
done

# Quote to escape globbing (a possible *)
echo "$reverse"
exit 0
}

main "$@"
