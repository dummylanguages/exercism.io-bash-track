#!/usr/bin/env bash

main () {
  for char in {A..Z}
  do
    [[ "${1^^}" == *"${char}"* ]] || { echo 'false'; exit 0; }
  done

  echo 'true'
  exit 0
}

main "$@"

