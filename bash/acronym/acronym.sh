#!/usr/bin/env bash

main () {
# Substituting all occurrences of dashes, underscores
# and asterisks by one space.
sentence=${@//[-_\*]/' '}
#sentence=${@//*(-_*)/ }

# Will hold the acronym
local acronym=''

for word in ${sentence}
do
  acronym+="${word:0:1}"
done

# Modify the case
echo ${acronym^^}
exit 0
}

main "$@"

