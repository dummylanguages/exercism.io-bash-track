#!/usr/bin/env bash

main () {
  # Cleaning the string from any non-alphanumeric character
  local isbn=${1//[![:alnum:]]}
  local check_digit=${isbn:9:1}
  local i j k
  local sum product

  # Input validation: long or short isbn
  if [[ ${#isbn} -ne 10 ]]
  then
    printf "%s" "false"
    exit 0
  # If the Check Digit is not X, or a number.
  elif [[ ${check_digit^^}  !=  "X" ]] && 
      ! [ "$check_digit"    -eq "$check_digit" ] 2> /dev/null
  then
    printf "%s" "false"
    exit 0
  fi

  # Iterate over the characters of the string except the last one.
  for ((i=0; i < ${#isbn} - 1; i++))
  do
    # If some digit is not a digit.
    if ! [ "${isbn:i:1}" -eq "${isbn:i:1}" ] 2> /dev/null
    then
      printf "%s" "false"
      exit 0
    else
      # Add the digits to the array.
      isbn_arr+=( "${isbn:i:1}" )
    fi
  done

  # Let's deal with the Check Digit.
  if [[ "${check_digit^^}" == 'X' ]]
  then
    # If it's an X (or x) we add a '10' to the end of the array.
    isbn_arr+=(10)
  else
    # If it's a number, we add the number.
    isbn_arr+=( "$check_digit" )
  fi

  for((j=10, k=0; j >= 1; j--, k++))
  do
    # 'product' is updated on each iteration
    product=$((j * isbn_arr[k]))
    # 'sum' is incremented
    ((sum += product))
  done

  if ((sum % 11 == 0))
  then
    printf "%s" "true"
  else
    printf "%s" "false"
  fi
}

main "$@"

