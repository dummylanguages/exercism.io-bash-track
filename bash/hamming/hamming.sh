#!/usr/bin/env bash

main () {
# Test if we have exactly 2 arguments.
if [[ ${#} -eq 2 ]]
then
  # Both strings to uppercase
  str1=${1^^}
  str2=${2^^}

  # Test if they have the same length.
  if [[ ${#str1} -eq ${#str2} ]]
  then
    # Test if both strings contains only letters.
    if [[ "${str1}${str2}" =~ ^[A-Z\ ]*$ ]]
    then
      # Test if they are equal.
      [[ ${str1} == ${str2} ]] && { echo 0; exit 0; }
      # If they're not equal, find the Hamming distance.
      hamming_distance
    else
      # If there is some non-letter, this will run.
      echo 1
      exit 0
    fi
  # Not the same length
  else
    echo "left and right strands must be of equal length"
    exit 1
  fi
# Not exactly 2 arguments.
else
  echo "Usage: hamming.sh <string1> <string2>"
  exit 1
fi
} # End of main

hamming_distance () {
  local differences=0
  for (( i=0; i < ${#str1}; i++ ))
  do
    [[ ${str1:$i:1} != ${str2:$i:1} ]] && (( differences++ ))
  done

  echo $differences
  exit 0
}

# call main with all of the positional arguments
main "$@"

