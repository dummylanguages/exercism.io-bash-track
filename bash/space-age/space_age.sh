#!/usr/bin/env bash

function years_in () {
  local planet=$1
  local seconds=$2
  local years=0
  # Associative arrays must be explicitely declared.
  declare -A planets=(
    [mercury]=0.2408467
    [venus]=0.61519726
    [earth]=1
    [mars]=1.8808158
    [jupiter]=11.862615
    [saturn]=29.447498
    [uranus]=84.016846
    [neptune]=164.79132
  )

  # If the planet is not in the list, print message and exit.
  if ! [[ "${!planets[*]}" == *"$planet"* ]]
  then
    echo 'not a planet'
    exit 1
  fi

  # Compute the years. The scale is 3, but printf rounds it to 2 decimals.
  years=$( printf %.2f \
    "$(echo "scale=7; $seconds/60/60/24/(365.25 * ${planets[$planet]})" | bc)"
  )

  # Print result and exit 0.
  echo "$years"
  exit 0
}

main () {
  local planet="${1,,}" # Store the planet name all lowercase.
  local seconds=$2

  # Transform seconds to earth years.
  years_in "$planet" "$seconds"
}

main "$@"

