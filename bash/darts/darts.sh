#!/usr/bin/env bash

main () {
  local x y radius
  x=$1
  y=$2
  regex='^[-]?[0-9]+\.?[0-9]*$'
  # ^       Match start of string
  # $       Match the end of the string
  # [0-9]+  Match one or more digit
  # [0-9]*  Match zero or more digits
  # [-]?    Match a leading - (the ? stands for zero or one occurrences of -)
  # \.?     Match a literal . (the dot gotta be escaped with a backslash)

  # Input validation
  if (($# != 2))
  then
    echo "Error: not nuff args"
    exit 1
  elif ! [[ "$x" =~ $regex ]] || ! [[ "$y" =~ $regex ]]
  then
    echo "Error: non-numeric args"
    exit 1
  fi

  # Pythagorean theorem: x^2 + y^2 = radius^2
  radius=$(echo "scale= 3; sqrt($x^2 + $y^2)" | bc)

  # Within arithmetic expansion, 1 is true, 0 is false.
  if (( $(echo "$radius > 10" | bc) )) # Outside outer circle.
  then
    echo 0
    exit 0
  elif (( $(echo "$radius <= 1" | bc) )) # Inside the inner circle.
  then
    echo 10
    exit 0
  elif (( $(echo "$radius <= 5" | bc) )) # Inside the middle circle.
  then
    echo 5
    exit 0
  elif (( $(echo "$radius <= 10" | bc) )) # Inside the outer circle.
  then
    echo 1
    exit 0
  fi
}

main "$@"

