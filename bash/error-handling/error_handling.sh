#!/usr/bin/env bash

# The following comments should help you get started:
# - Bash is flexible. You may use functions or write a "raw" script.
#
# - Complex code can be made easier to read by breaking it up
#   into functions, however this is sometimes overkill in bash.
#
# - You can find links about good style and other resources
#   for Bash in './README.md'. It came with this exercise.
#
#   Example:
#   # other functions here
#   # ...
#   # ...
#
main () {
# If exactly 1 argument is passed, print it and return 0
[[ "$#" -eq 1 ]] && { echo "Hello, ${1}"; return 0; }

# Return 1 and error message if more than 1 argument or no argument is passed
echo "Usage: error_handling.sh <person>"
return 1
} # End of main
#
#   # call main with all of the positional arguments
main "$@"
#
# *** PLEASE REMOVE THESE COMMENTS BEFORE SUBMITTING YOUR SOLUTION ***
