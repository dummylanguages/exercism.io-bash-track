#!/usr/bin/env bash

main () {
  local RNA=$1
  declare -A codons
  codons=(
    ['AUG']=Methionine
    ['UUU']=Phenylalanine
    ['UUC']=Phenylalanine
    ['UUA']=Leucine
    ['UUG']=Leucine
    ['UCU']=Serine
    ['UCC']=Serine
    ['UCA']=Serine
    ['UCG']=Serine
    ['UAU']=Tyrosine
    ['UAC']=Tyrosine
    ['UGU']=Cysteine
    ['UGC']=Cysteine
    ['UGG']=Tryptophan
    ['UAA']=Stop
    ['UAG']=Stop
    ['UGA']=Stop
  )
  local codon         # Substring to store the codon.
  local translation   # Array to hold the answer.

  # Iterate over the RNA sequence codon by codon. (1 codon = 3 nucleotides)
  for ((i=0; i < ${#RNA}; i+=3))
  do
    codon=${RNA:i:3}

    # Iterate over the keys of the 'codons' associative array.
    for c in "${!codons[@]}"
    do
      # Check for a non-valid codon.
      if ! [[ "${!codons[*]}" == *"$codon"* ]]
      then
        printf "%s\n" "Invalid codon" 
        exit 1
      fi

      # When the current codon matches any of the keys in 'codons'.
      if [ "$c" == "$codon" ]
      then
        # Terminate if it's a STOP codon.
        if [ ${codons[$c]} == 'Stop' ]
        then
          break 2
        else
          translation+=("${codons[$c]}")
        fi
      fi
    done
  done

  # Finally, print the protein translation. 
  # 'echo' is handy here, it separates array elements with spaces.
  echo ${translation[@]}
}

main "$@"

