#!/usr/bin/env bash

main () {
  gifts=(
    'a Partridge in a Pear Tree'
    'two Turtle Doves'
    'three French Hens'
    'four Calling Birds'
    'five Gold Rings'
    'six Geese-a-Laying'
    'seven Swans-a-Swimming'
    'eight Maids-a-Milking'
    'nine Ladies Dancing'
    'ten Lords-a-Leaping'
    'eleven Pipers Piping'
    'twelve Drummers Drumming'
  )
  
  days=(
    'first'
    'second'
    'third'
    'fourth'
    'fifth'
    'sixth'
    'seventh'
    'eighth'
    'ninth'
    'tenth'
    'eleventh'
    'twelfth'
  )

  # Stuff holds the each day gitfs.
  local stuff

  # The arguments set the low and high boundaries for the verses.
  local low=$1
  local high=$2

  # Adjust the boundaries to the zero-based arrays.
  ((low--))
  ((high--))

  # Iterate between the low and high boundaries (both adjusted).
  for ((i=$low; i <= $high; i++))
  do
    stuff='' # Empty it at the start of every day.

    # Add gifts incrementally.
    for ((j=$i; j >= 0; j-- ))
    do
      # Day 1: only the partridge (low was adjusted to 0).
      if [[ $i -eq 0 ]]
      then
        stuff+="${gifts[0]}." # Drop the 'and' before the partridge.

      # Last gift of the day (the partridge).
      elif [[ $j -eq 0 ]]
      then
        stuff+="and ${gifts[$j]}."  # Add a dot at the end of the last gift.
      else
        stuff+="${gifts[$j]}, " # Add a comma and a space to separate gifts.
      fi
    done

    printf "On the %s day of Christmas my true love gave to me: %s\n" \
          "${days[$i]}" "${stuff}"
  done
}

main "$@"

