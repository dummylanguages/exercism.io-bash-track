#!/usr/bin/env bash

divisors ()
{
  local num=$1
  local divisors

  for ((i=2; i <= num; i++))
  do
    if ((num % i == 0))
    then
      divisors+=( $i )
    fi
  done
  
  printf "%s" "${divisors[*]}"
}

coprime ()
{
  local a_divisors m_divisors
  local num num2
  local a m
  a=$1
  m=$2

  a_divisors=( $(divisors $a) )
  m_divisors=( $(divisors $m) )

  for num in "${a_divisors[@]}"
  do
    for num2 in "${m_divisors[@]}"
    do
      if ((num == num2))
      then
        printf "a and m must be coprime." 
        exit 1
      fi
    done
  done
}

format ()
{
  local string=$1
  local formatted

  while read -n5 five_chars
  do
    if ((${#five_chars} == 5))
    then
      formatted+="$five_chars "
    else
      formatted+="$five_chars"
    fi
  done <<< $string

  echo $formatted
}

# Return the numeric equivalent of a character or viceversa.
equivalent ()
{
  local char=$1
  local alphabet
  alphabet=($(echo {a..z}))

  for((i=0; i < ${#alphabet[@]}; i++))
  do
    # If the character is a letter.
    if [[ "$char" =~ [[:alpha:]] ]] && [[ "$char" == "${alphabet[i]}" ]]
    then
      printf "%i" "$i"
      return 0
    # If the character is a number.
    elif [[ "$char" =~ [[:digit:]] ]] && [[ "$char" -eq "$i" ]]
    then
      printf "%s" "${alphabet[i]}"
      return 0
    fi
  done
}

encode ()
{
  local a b string
  local arr temp char encoded output
  a=$1
  b=$2
  string=$3
  
  # Create array from the user input string.
  while read -n1 char
  do
    # When we get to the newline character added by while/read, bail.
    if [[ $char == $'\n' ]]
    then
      break
    fi
    arr+=($char)
  done <<< "$string"

  for char in "${arr[@]}"
  do
    if [[ "$char" =~ [[:digit:]] ]]
    then
      output+="$char"
    else
      # Formula: encode(x) = (ax + b) mod m
      if temp=$(equivalent "$char")
      then
        # Get the encoded digit.
        encoded="$(( (temp * a + b ) % 26 ))"
        # Get the character equivalent of the encoded digit.
        output+=$(equivalent $encoded)
      fi
    fi
  done
    
  format $output
 }

mmi ()
{
  local a=$1
  local m=$2
  local i

  for ((i=0; i <= $m; i++))
  do
    if (( a * i % m == 1))
    then
      printf "%i" "$i"
      return
    fi
  done
}

decode ()
{
  local a b m string  # Positional parameters
  local char arr      # To make an array out of the string
  local mmi i output numeric_equivalent decoded 
  a=$1
  b=$2
  m=$3
  string=$4

  # Create array from the encoded string.
  while read -n1 char
  do
    # When we get to the newline character added by while/read, bail.
    if [[ $char == $'\n' ]]
    then
      break
    fi
    arr+=($char)
  done <<< "$string"

  mmi=$(mmi $a $m)

  for ((i=0; i < ${#arr[@]}; i++))
  do
    char=${arr[i]}

    if [[ "$char" =~ [[:digit:]] ]]
    then
      output+="$char"
    else
      if numeric_equivalent=$(equivalent "$char")
      then
        # D(y) = a^-1(y - b) mod m
        # 'mmi' is the Modular Multiplicative Inverse of a: a^-1
        while ((numeric_equivalent - b < 0))
        do
          ((numeric_equivalent += m))
        done

        decoded=$(( mmi * (numeric_equivalent - b) % m ))

        # Get the character equivalent of the encoded digit.
        output+=$(equivalent $decoded)
      fi
    fi
  done

  echo $output
}

main ()
{
  local alphabet
  local a b           # Encryption keys.
  local m             # Size of the alphabet.
  local input=${4,,}  # User input to lowercase.

  alphabet=$(printf "%s" {a..z})
  a=$2
  b=$3
  m=${#alphabet}
  input=${input//[![:alnum:]]} # Delete non-alphanumeric symbols.

  # User input validation
  coprime "$a" "$m"

  case $1 in
  encode)
    encode $a $b $input
    ;;
  decode)
    decode $a $b $m $input
    ;;
  esac
}

main "$@"
