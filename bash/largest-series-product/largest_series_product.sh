#!/usr/bin/env bash

main () {
  # your main function code here
  local number=$1     # String of digits.
  local slice=$2      # Length of the substring.
  local digits=${#1}  # Amount of digits of the string.
  local largest       # Store the largest product.
  local temp=1        # Store the temporary product.

  # Filtering invalid user input.
  if ((slice > digits)) || [[ $number == '' ]]
  then
    printf "%s\n" "span must be smaller than string length"    
    exit 1
  elif ((slice < 0))
  then
    printf "%s\n" "span must be greater than zero"
    exit 1
  fi

  # Iterate over the digits on the string.
  for ((i=0; i <= digits - slice; i++))
  do
    for ((j=i; j <= i + slice - 1; j++))
    do
      digit=${number:j:1}

      # More user-input filtering.
      if ! [[ $digit =~ [[:digit:]] ]]
      then
        printf "%s\n" "input must only contain digits"
        exit 1
      fi

      (( temp *= digit ))
    done

    # If the new product is greater than the old product, become the largest.
    if ((temp > largest))
    then
      largest=$temp
    fi

    # Reset the 'temp' variable.
    temp=1
  done
  
  # Finally, print the largest product of the series.
  printf "%i\n" "$largest"
}

main "$@"

