#!/usr/bin/env bash
change_base ()
{
  local input_base=$1
  local orig_num="$2"
  local output_base=$3
  local orig_arr
  local output_arr
  local inverted_arr
  local num
  local exp
  local i j

  # Convert original number to array (Easier to iterate over)
  read -ra orig_arr <<< "$orig_num" 

  # 'num' holds the value of the number
  for ((i=0, exp="${#orig_arr[@]}" - 1; exp >= 0; exp--, i++))
  do
    temp=${orig_arr[i]}
    ((num += temp * input_base ** exp))
  done
  
  if ((num >= output_base))
  then
    for ((j=0; num >= output_base; j++))
    do
      remainder=$((num % output_base))
      quotient=$((num / output_base))
      ((num /= output_base))
      output_arr[j]="$remainder"
    done

    # Tag at the end the last quotient
    output_arr=( ${output_arr[@]} $quotient )
  else
    output_arr=( $num )
  fi

  # Let's invert the array order.
  for elem in "${output_arr[@]}"
  do
    inverted_arr=( $elem ${inverted_arr[@]} )
  done

  # Print an array elements separated by a space.
  printf "%s" "${inverted_arr[*]}"
}

main () {
  local input_base=$1
  local orig_num="$2"
  local output_base=$3

  local n i       # Loop variables
  local digit     # Just for convenience
  local orig_arr  # The original digits in array form
  local zero      # Flag to check if the input number is zero

   # User input validation: Check the bases
  for n in $input_base $output_base
  do
    if ((n <= 1))
    then
      printf "Base can't be 1 or less.\n"
      exit 1
    fi
  done

  read -ra orig_arr <<< "$orig_num" # Easier to iterate over an array.
  zero='true' # Let's set up a flag before the loop.

  # A bit more of input validation: Check the digits
  for ((i=0; i < "${#orig_arr[@]}"; i++))
  do
    digit=${orig_arr[i]}

    if ((digit != 0))   # If all digits are zero, the `zero` flag is not unset
    then
      zero='false'
    fi

    # Invalid positive digit: The number has a digit larger than the base.
    if ((digit >= input_base))
    then
      printf "Digits can't be larger than the base."
      exit 1
    fi

    # In case any digit is negative, exit.
    if ((digit < 0))
    then
      printf "No negative numbers my man\n"
      exit 1
    fi
  done
  
  if [[ "$zero" == 'true' ]]
  then
    # If at this point the flag is still 'true', the number is 0
    exit 0
  fi

  change_base "$input_base" "$orig_num" "$output_base" 
}

main "$@"

