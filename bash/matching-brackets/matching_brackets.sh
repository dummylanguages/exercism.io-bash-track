#!/usr/bin/env bash

closing_match () {
  case "$1" in
    '[')
      echo ']'
      ;;
    '(')
      echo ')'
      ;;
    '{')
      echo '}'
      ;;
  esac
}

# Delete a pair of brackets at a given index.
delete_brackets () {
  local index=$1
  local string=$2
  local new_string

  # We rebuild a new string without the matching brackets.
  new_string=${string:0:$index}     # 1st half
  new_string+=${string:$index + 2}  # 2nd half

  # "Return" the string without the matching brackets.
  echo "$new_string"
}

# Scan a string for consecutive matching brackets.
find_matching_brackets () {
  local string=$1
  local brackets='{[('
  local char

  # Let's iterate over the string: left to right.
  for ((i=0; i < ${#string}; i++))
  do
    
    # Setting up char of current iteration.
    char=${string:$i:1}

    # If the current char is not an opening bracket, continue
    if ! [[ "$brackets" == *"$char"* ]]
    then
      continue
    fi

    # Get the matching bracket of the current bracket.
    closing_bracket=$(closing_match $char)

    # Set the character next to the current char.
    next=${string:$i + 1:1}

    # If the character next to the current, matches its closing bracket.
    if [[ "$next" == "$closing_bracket" ]]
    then
      # Return the index of the bracket that has closing match right after.
      echo "$i" 
      return 0
    fi
  done

  # Return non-zero code if there are no matching brackets.
  return 1
}

main () {
  # This pattern will match all brackets symbols (Some gotta be escaped)
  local pattern='\[\]{\}()' # Put the pattern in a var, it looks better.

  # Clean string from any non-bracket character.
  local string=${1//[!$pattern]}
  local index

  while [ ${#string} -gt 0 ]
  do
    if index=$(find_matching_brackets "$string")
    then
      string=$(delete_brackets "$index" "$string")
    else
      echo "false"
      exit 0
    fi
  done

  echo "true"
  exit 0
}

main "$@"

