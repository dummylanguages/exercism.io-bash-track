#!/usr/bin/env bash

throw_die ()
{
  # RANDOM generates a number between 0 and 32767.
  randie=$(($RANDOM % 6 + 1)) # 6 upper range, 1 lower range.
  echo "$randie"
}

roll_four_dice ()
{
  local four_dice
  
  for d in {1..4}
  do
    # Using this method because I know the elements, 1 to 6, contain no spaces.
    four_dice+=( $(throw_die) )
  done

  echo "${four_dice[@]}"
}

generate_character ()
{
  declare -A character
  local hitpoints
  local abilities=(
    strength
    dexterity
    constitution
    intelligence
    wisdom
    charisma
  )
  local rolling
  local a roll value ability # Loop variables
  local min=7
  local sum
  local constitution_mod

  for a in "${abilities[@]}"
  do
    # Let's get a roll of 4 dice, and make it an array.
    rolling=( $(roll_four_dice) )

    # Iterate over the throws of the rolling to find the minimum value.
    for roll in "${!rolling[@]}"
    do
      if (( rolling[roll] < min ))
      then
        min=${rolling[roll]}
        delete=$roll
      fi
    done

    # Delete the minimum value from the rolling array.
    unset rolling[delete]

    # Reset the sum of values.
    sum=0
    
    # Iterate over the leftover values to sum them.
    for value in "${rolling[@]}"
    do
      ((sum += value))
    done

    character[$a]=$sum
  done
  
  constitution_mod=$(generate_modifier "${character['constitution']}" )
  # The 'hitpoints' are calculated separately, then added to the dictionary.
  character['hitpoints']=$(( 10 + constitution_mod ))

  for ability in "${!character[@]}"
  do
    echo "$ability ${character[$ability]}"
  done
}

generate_modifier ()
{
  #declare -A character
  #local character_string
  #local temp_array

  # The function 'generate_character' outputs the character as a string.
  #character_string=$(generate_character)

  # Convert the string of the 'character' into an associative array.
  #while read ability score
  #do
    #character["$ability"]="$score"
  #done <<< "$character_string"
    
  #echo "$(( (character[constitution] - 10) / 2 ))"

  # No need for the stuff above, the constitution is given... lol
  local constitution
  constitution=$1

  # If-statement runs when arithmetic expression evaluates to non-zero: 'true'. 
  if ((constitution % 2))
  then
    # When constitution is odd, test evaluates to non-zero=true,
    # so we subtract 11 to compensate when truncating decimals; it rounds down.
    echo "$(( (constitution - 11) / 2 ))"
  else # This clause runs when the test is zero=false. (even number)
    echo "$(( (constitution - 10) / 2 ))"
  fi
}

main () {
  local character
  case "$1" in
    generate)
      character=$(generate_character)
      echo -e "$character"
      ;;
    modifier)
      generate_modifier "$2"
      ;;
  esac
}

main "$@"

