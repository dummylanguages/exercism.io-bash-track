#!/usr/bin/env bash

function odd () {
  number=$(( 3 * $number + 1 ))
  ((steps++)) # Increase the step counter.
}

function even () {
  number=$(( $number / 2 ))
  ((steps++)) # Increase the step counter.
}

main () {
  number=$1 # Positional parameters can't be assigned to.
  steps=0
  
  # If argument is zero or negative (less than 1)
  [[ $1 -lt 1 ]] && { echo 'Error: Only positive numbers are allowed'; exit 1; }

  # Collatz Conjecture: No matter the number, we'll always get 1.
  # If the number is odd
  while [ $number != 1 ]
  do
    if [[ $(( $number % 2 )) -eq 0 ]]
    then
      # Call the even function 
      even
    else
      # Invoke the odd function 
      odd
    fi
  done
  
  # Show the number of steps and return a 0 exit code.
  echo $steps
  exit 0
}

main "$@"

