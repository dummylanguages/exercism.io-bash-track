#!/usr/bin/env bash

main () {
local limit=$1
shift # Delete $1 from the positional parameters array.
local numbers=("$@") # All positional parameters except $1.
local multiple
local multiples
local repeated # Flag for repeated multiples.
local sum=0

for n in "${numbers[@]}"
do
  #echo "* * * Number: $n * * *" # DELETE ME!
  # Sequential loop to create multiples.
  for ((i=1; i < limit; i++))
  do
    # Reset the flag for repeated multiples.
    repeated=0
    # Compute the next multiple.
    multiple=$((n*i))
    #echo "Multiple: $multiple" # DELETE ME!

    # Iterate over the multiples array.
    for m in "${multiples[@]}"
    do
      if ((m == multiple))
      then
        # If the multiple is already in the list, flag it.
        repeated=1
        #echo -e "MULTIPLE REPEATED!!! $m = $multiple\n" # DELETE ME!
        break
      fi
    done

    if ((repeated == 1))
    then
      continue
    fi

    # If the multiple is less than the limit and is not repeated.
    if ((multiple < limit))
    then
      # Add the multiple to the array of multiples.
      multiples+=("$multiple")
      # And increment the sum.
      ((sum += multiple))
      #echo -e "Adding $multiple, Sum: $sum\n" # DELETE ME!
    else
      #echo -e "Multiple not less than limit... bailing\n" # DELETE ME!
      break
    fi
  done
done

echo "$sum"
}

main "$@"

