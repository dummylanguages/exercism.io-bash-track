#!/usr/bin/env bash

function left () {
  if (( $bottles == 0 ))
  then
    printf "no more bottles"
  elif (( $bottles == 1 ))
  then
    printf "${bottles} bottle"
  else
    printf "${bottles} bottles"
  fi
}

function sing () {
  if (( $bottles == 0 ))
  then
    echo "No more bottles of beer on the wall, no more bottles of beer."
    echo "Go to the store and buy some more, 99 bottles of beer on the wall."
    exit 0
  fi

  # First verse.
  echo "$(left) of beer on the wall, $(left) of beer."

  # If there's only one bottle, the second verse says ...Take IT down...
  if (( $bottles == 1)); then last='it'; fi

  # Decrement the number of bottles after taking one down.
  ((--bottles))

  # Second verse.
  echo "Take ${last:-one} down and pass it around, $(left) of beer on the wall."
}

main () {
  bottles=$1

  # If the user inputs wrong amount of arguments.
  if [[ ${#@} -gt 2 ]] || [[ ${#@} -lt 1 ]]
  then
    echo "1 or 2 arguments expected"
    exit 1

  # If the user inputs high and low ranges. 
  elif [ ${#@} -eq 2  ]
  then
    # The user messed up the high and low ranges.
    if [[ ${1} -lt ${2} ]]
    then
      echo "Start must be greater than End"
      exit 1
    fi

    # Keep singing until the lower range is reached.
    while (( bottles >= ${2} ))
    do
      sing
      echo ''
    done

  # The user only passed one argument.
  else
    sing
    exit 0
  fi
}

main "$@"

