#!/usr/bin/env bash

function square_of_sum () {
  local counter=1
  local sum=0

  while [ $counter -le $2 ]
  do
    (( sum += $counter ))

    (( counter++ ))
  done

  #echo "$sum ^ 2" | bc
  echo $(( $sum ** 2 ))
  exit 0
}

function sum_of_squares () {
  local counter=1
  local square=0
  local sum_of_squares=0

  while [ $counter -le $2 ] 
  do
    square=$(( $counter ** 2 ))
    (( sum_of_squares += $square ))

    (( counter++ ))
  done

  echo $sum_of_squares
  exit 0
}

function difference () {
  local sum_of_squares=$( sum_of_squares "$@" )
  local square_of_sum=$( square_of_sum "$@" )

  #echo "square_of_sum: $square_of_sum, sum_of_squares: $sum_of_squares"
  echo $(( $square_of_sum - $sum_of_squares ))
  exit 0
}

main () {
  # Positional parameter $2 selects:
  #   sum_of_squares
  #   square_of_sum
  #   difference

  case $1 in
  sum_of_squares)
    sum_of_squares "$@" # Must pass the positional params.
    ;;

  square_of_sum)
    square_of_sum "$@" # Must pass the positional params.
    ;;

  difference )
    difference "$@"   # Must pass the positional params.
    ;;
  esac
}

main "$@"

