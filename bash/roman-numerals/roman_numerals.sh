#!/usr/bin/env bash

main () {
  local arabic=$1
  local roman  # Array of Roman numerals.
  local left center right # Three Roman numerals.
  local result # String to build the result with Roman numerals.

  roman=(I V X L C D M)

  while read -n1 digit
  do
    digits=( $digit ${digits[@]} )
  done <<< $arabic

  for ((i=0, j=0; i < ${#digits[@]}; i++, j+=2))
  do
    left=${roman[j]}
    center=${roman[j+1]}
    right=${roman[j+2]}
    
    case ${digits[i]} in
    0)
      continue
      ;;
    1)
      result="${left}${result}"
      ;;
    2)
      result="${left}${left}${result}"
      ;;
    3)
      result="${left}${left}${left}${result}"
      ;;
    4)
      result="${left}${center}${result}"
      ;;
    5)
      result="${center}${result}"
      ;;
    6)
      result="${center}${left}${result}"
      ;;
    7)
      result="${center}${left}${left}${result}"
      ;;
    8)
      result="${center}${left}${left}${left}${result}"
      ;;
    9)
      result="${left}${right}${result}"
      ;;
    esac
  done

  echo $result
}

main "$@"

