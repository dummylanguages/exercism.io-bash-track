#!/usr/bin/env bash

# Find the next prime factor.
next_prime () {
  local number=$1
  
  # Iterate over all numbers from 2 to half the number.
  for ((i=2; i <= $((number / 2)); i++))
  do
    # If you find a factor, print it and exit from the function.
    if ((number % i == 0))
    then
      echo "$i"
      exit 0
    fi
  done

  # If no factor is found, the number itself is returned.
  echo "$number"
  exit 0
}

main () {
  local num=$1
  local factors # Hold the list of prime factors.
  local prime_factor # Store the current factor.
  
  while [ "$num" -gt 1 ]
  do
    prime_factor=$(next_prime "$num")
    ((num /= prime_factor))

    # Concatenate the factor, and a space at the end.
    factors+="$prime_factor "
  done
    
  echo $factors  # Without double quotes, the trailing space is gone.
  
  return 0
}

main "$@"

