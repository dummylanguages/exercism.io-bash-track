#!/usr/bin/env bash

main () {
  # your main function code here
  local digits=${1// } # Number with whitespace removed.
  local digits_arr
  local temp sum

  # User input validation.
  if [[ $digits == '0' ]]
  then
    echo 'false'
    exit 0
  fi

  # If we remove also the digits, nothing must be left over.
  if [[ ${digits//[[:digit:]]} != '' ]]
  then
    echo 'false'
    exit 0
  fi

  # Convert the string to an array of digits.
  while read -n1
  do
    digits_arr+=($REPLY)
  done <<< "$digits"

  # Iterate over every other digit, starting from the right.
  for ((i=-2; i >= -${#digits}; i-=2))
  do
    temp="${digits_arr[i]}"

    if ((temp * 2 > 9))
    then
      temp=$(( (temp * 2) - 9 ))
      digits_arr[i]="$temp"
    else
      temp=$((temp * 2))
      digits_arr[i]="$temp"
    fi
  done

  for d in ${digits_arr[@]}
  do
    ((sum+=d))
  done

  if ((sum % 10 == 0))
  then
    echo 'true'
  else
    echo 'false'
  fi
}

main "$@"

